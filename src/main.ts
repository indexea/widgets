import { init, addMessages, getLocaleFromNavigator } from "svelte-i18n";

import './main.scss';
import SearchBox from './SearchBox.svelte'
import SearchDialog from './SearchDialog.svelte'
import RecommBox from './RecommBox.svelte'
import QueryBox from './QueryBox.svelte'
import en from "./locales/en.json";
import zhCN from "./locales/zh-CN.json";

//Internationalization initialization
addMessages("en", en);
addMessages("zh-CN", zhCN);

init({
    fallbackLocale: "zh-CN",
    initialLocale: getLocaleFromNavigator(),
});

export { SearchBox, SearchDialog, RecommBox, QueryBox }