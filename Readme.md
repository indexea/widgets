This is the repository for all Indexea web components

## 4 widgets

![widgets](/widgets.jpg)

## Installation

```shell
$ npm i @indexea/widgets
or
$ yarn add @indexea/widgets
```

## Usage

### SearchBox

```javascript
import { SearchBox } from '@indexea/widgets'

const app = document.getElementById('app')

new SearchBox({
  target: app,
  props: {
    widget: '[widget ident]',
    endpoint: 'https://api.indexea.com/v1' // Optional, change it in private cloud
  }
})
```

### SearchDialog

```javascript
import { SearchDialog } from '@indexea/widgets'

const app = document.getElementById('app')

new SearchDialog({
  target: app,
  props: {
    widget: '[widget ident]',
    endpoint: 'https://api.indexea.com/v1' // Optional, change it in private cloud
  }
})
```

### QueryBox

```javascript
import { QueryBox } from '@indexea/widgets'

const app = document.getElementById('app')

new QueryBox({
  target: app,
  props: {
    widget: '[widget ident]',
    endpoint: 'https://api.indexea.com/v1', // Optional, change it in private cloud
    xxxx: xxxx // Other args
  }
})
```

### RecommBox

```javascript
import { RecommBox } from '@indexea/widgets'

const app = document.getElementById('app')

new RecommBox({
  target: app,
  props: {
    widget: '[recomm ident]',
    endpoint: 'https://api.indexea.com/v1', // Optional, change it in private cloud
    xxxx: xxxx // Other args
  }
})
```
