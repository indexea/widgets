import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import sveltePreprocess from 'svelte-preprocess'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    mainFields: ['svelte', 'module', 'main'],
  },
  build: {
    lib: {
      entry: "src/main.ts",
      name: "@indexea/widgets",
      fileName: 'widgets',
    },
  },
  plugins: [
    svelte({
      emitCss: false,
      preprocess: sveltePreprocess({ postcss: true })
    })],
})
